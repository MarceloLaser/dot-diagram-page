const { src, dest, series } = require('gulp');
var del = require('del');
var webserver = require('gulp-webserver');

function buildCss(cb) {
    return src('node_modules/bootstrap/dist/css/*.min.css')
        .pipe(dest('public/css'));
}

function buildJS(cb) {
    return src('node_modules/jquery-mousewheel/*.js')
        .pipe(src('node_modules/jquery-color/*.js'))
        .pipe(src('node_modules/popper.js/dist/*.min.js'))
        .pipe(src('node_modules/bootstrap/dist/js/*.js'))
        .pipe(src('node_modules/jquery/dist/*.min.js'))
        .pipe(dest('public/js'));
}

function locally() {
    return src('public')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: true
        }));
}

exports.build = series(buildCss, buildJS);
exports.serve = locally;
exports.default = series(buildCss, buildJS);
